
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Usuario
 */
public class DB {

    private Connection conn;
    // Librería de MySQL
    public String driver = "com.mysql.cj.jdbc.Driver";

    // Nombre de la base de datos
    public String database = "usuarios";

    // Host
    public String hostname = "localhost";

    // Puerto
    public String port = "3306";

    // Ruta de nuestra base de datos (desactivamos el uso de SSL con "?useSSL=false")
    public String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false&serverTimezone=UTC";

    // Nombre de usuario
    public String username = "root";

    // Clave de usuario
    public String password = "root";

    public DB() throws SQLException {
        try {
            Class.forName(driver);

        } catch (Exception e) {
            e.printStackTrace();
        }
        conn = DriverManager.getConnection(url, username, password);

    }

    public User selectUsuario(String userName) throws SQLException {
        User user = null;

        String query = "SELECT `users`.`idusuario`,\n"
                + "    `users`.`nombre`,\n"
                + "    `users`.`apellido`,\n"
                + "    `users`.`edad`\n"
                + "FROM `usuarios`.`users`;";
        ResultSet result = null;

        Statement statement = this.conn.createStatement();
        result = statement.executeQuery(query);
        int id = 0, edad = 0;
        String nombre = "", apellido = "";
        while (result.next()) {
            id = result.getInt("idusuario");
            nombre = result.getString("nombre");
            apellido = result.getString("apellido");
            edad = result.getInt("edad");
            user = new User(id, edad, nombre, apellido);
        }
        statement.close();

        return user;
    }

    public boolean doesUserExist(String userName) throws SQLException {

        boolean exist = false;
        String query = "SELECT * FROM usuarios.users\n"
                + "WHERE nombre LIKE '" + userName + "';";

        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
            exist = true;
        }
        return exist;

    }

}
