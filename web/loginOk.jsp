<%-- 
    Document   : loginOk
    Created on : 19-nov-2019, 13:49:19
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logged in successfully</title>
    </head>
    <body>
        <h1>${user.getNombre()}, te has logueado!</h1>
        <table border="3">
            <tr>
                <td><b>ID</b></td>
                <td><b>Nombre</b></td>
                <td><b>Apellido</b></td>
                <td><b>Edad</b></td>
            </tr>
            <tr>
                <td>${user.getId()}</td>
                <td>${user.getNombre()}</td>
                <td>${user.getApellido()}</td>
                <td>${user.getEdad()}</td>   
            </tr> 
        </table>
    </body>
</html>
